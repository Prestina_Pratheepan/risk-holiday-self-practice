package holiday.self.practicesQ03;

public class XwithXs {

	public static void main(String[] args) {
			
			for (int i = 0; i < 9; i++) {
				
				for(int j = 0; j < 9; j++) {
					
					if(i==j || i+j==9-1) {
						System.out.print("X");
					}
					else {
						System.out.print(" ");
					}
				}
				System.out.println();
			}
		}
	}

/*
 
X       X
 X     X 
  X   X  
   X X   
    X    
   X X   
  X   X  
 X     X 
X       X

 */


