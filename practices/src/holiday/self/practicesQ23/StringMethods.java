package holiday.self.practicesQ23;

public class StringMethods {
	
	public static void main(String[] args) {
		
			String word = "School.of.Progressive. Rock";
		
				System.out.println("A. Word.Length() : "+word.length());
				System.out.println("B. Word.Substring() : "+word.substring(22));
				System.out.println("C. Word.Substring() : "+word.substring(22, 24));
				System.out.println("D. Word.IndexOf() : "+word.indexOf("oo"));
				System.out.println("E. Word.toUpperCase() : "+word.toUpperCase());
				System.out.println("F. Word.LastIndexOf() : "+word.lastIndexOf("o"));
				System.out.println("G. Word.IndexOf() : "+word.indexOf("ok"));

		}
	}

/*
A. Word.Length() : 27
B. Word.Substring() :  Rock
C. Word.Substring() :  R
D. Word.IndexOf() : 3
E. Word.toUpperCase() : SCHOOL.OF.PROGRESSIVE. ROCK
F. Word.LastIndexOf() : 24
G. Word.IndexOf() : -1

*/


