package holiday.self.practicesQ29;
import java.util.Scanner;
public class NumeralSum {
	
	public static void main(String[] args) {
			
			Scanner scan = new Scanner(System.in);
			System.out.print("Enter the word : ");
			String word = scan.nextLine();
			
			int num = 0;
			
			for (int i = 0; i < word.length(); i++) {
				if (Character.isDigit(word.charAt(i))) {
					num = num + Character.getNumericValue(word.charAt(i));
				}
			}
			System.out.println("Sum : "+num);
		}
	}

/*
Enter the word : P2S4JEEV
Sum : 6
 */

