package holiday.self.practicesQ13;

import java.util.Scanner;
 
public class RightAngle {
	
public static void main(String[] args){

	
			Scanner scan = new Scanner(System.in);
			
			System.out.print("ENTER SIDE A : ");
			double sideA = scan.nextInt();
			System.out.print("ENTER SIDE B : ");
			double sideB = scan.nextInt();
			System.out.print("ENTER SIDE C : ");
			double sideC = scan.nextInt();
			System.out.print("Is Right Angle Triangle : " + ValidTriangle(sideA, sideB, sideC));
		}

		public static boolean ValidTriangle(double sideA, double sideB, double sideC) {
			return (Math.pow(sideA, 2)+Math.pow(sideB, 2) == Math.pow(sideC, 2)) || 
					(Math.pow(sideA, 2)+Math.pow(sideC, 2) == Math.pow(sideB, 2)) ||
					(Math.pow(sideB, 2)+Math.pow(sideC, 2) == Math.pow(sideA, 2));
		}
	}
	
/*
ENTER SIDE A : 3
ENTER SIDE B : 4
ENTER SIDE C : 5
Is Right Angle Triangle : true
*/