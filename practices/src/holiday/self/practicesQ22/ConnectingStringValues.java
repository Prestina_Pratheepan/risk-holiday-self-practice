package holiday.self.practicesQ22;
import java.util.Scanner;
public class ConnectingStringValues{

	public static void main(String[] args) {
		
			Scanner scan = new Scanner(System.in);
			
			System.out.print("INPUT WORD 1 : ");
			String word1 = scan.nextLine();
			System.out.print("INPUT WORD 2 : ");
			String word2 = scan.nextLine();

			char fc1=word1.charAt(0);
			char fc2=word2.charAt(0);
			char lc1=word1.charAt(word1.length()-1);
			char lc2=word2.charAt(word2.length()-1);
			System.out.print("OUTPUT : " + connect(word1,word2,fc1,fc2,lc1,lc2));
		}

		public static boolean connect(String word1, String word2,char fc1,char fc2,char lc1,char lc2) {
			return (lc1==fc2 || lc2==fc1 || (word1.length()==word2.length()));
		}
	}
	
/*
INPUT WORD 1 : JAPAN
INPUT WORD 2 : CHINA
OUTPUT : true
*/

/*
INPUT WORD 1 : GERMANY
INPUT WORD 2 : LONDON
OUTPUT : false
*/