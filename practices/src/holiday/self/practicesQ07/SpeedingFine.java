package holiday.self.practicesQ07;

public class SpeedingFine {
	
	public static void main(String[] args) {
		
		int speed_1 = 50;
		int speed_2 = 30;
		int speed_3 = 60;
		
		int limit_1 = 35;
		int limit_2 = 25;
		int limit_3 = 45;
		
		int fine_1 = (speed_1 - limit_1)*20;
		int fine_2 = (speed_2 - limit_2)*20;
		int fine_3 = (speed_3 - limit_3)*20;
		
		System.out.println("The fine for driving at" + speed_1 + "mph on a " + limit_1 + "mph road is" + fine_1+ "dollars");
		System.out.println("The fine for driving at" + speed_2 + "mph on a " + limit_2 + "mph road is" + fine_2+ "dollars");
		System.out.println("The fine for driving at" + speed_3 + "mph on a " + limit_3 + "mph road is" + fine_3+ "dollars");
	}
	
}

/*
 The fine for driving at50mph on a 35mph road is300dollars
The fine for driving at30mph on a 25mph road is100dollars
The fine for driving at60mph on a 45mph road is300dollars
 */


