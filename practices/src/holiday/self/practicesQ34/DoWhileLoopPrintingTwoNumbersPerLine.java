package holiday.self.practicesQ34;

public class DoWhileLoopPrintingTwoNumbersPerLine {
		
		public static void main(String[] args) {
			
			int i = 1;
			int num = 10;
			
			do {
				
				int odd =  i *2;
				int even = odd -1 ;
				
				String value = String.format("%03d",even );
				String value1 = String.format("%03d",odd );
				
				System.out.println(value+".0"+ "," +value1+".0");
				
				i++;
			}
			
			while (i <= num/2);

			
		}

	}

	/*
001.0,002.0
003.0,004.0
005.0,006.0
007.0,008.0
009.0,010.0
	*/


