package holiday.self.practicesQ12;

import java.util.Scanner;

public class TriangleValidity {

public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
				System.out.print("SIDE A: ");
				double sideA = scan.nextInt();
				System.out.print("SIDE B: ");
				double sideB = scan.nextInt();
				System.out.print("SIDE C: ");
				double sideC = scan.nextInt();
				
				
				System.out.print("Is Validity Triangle : " + ValidTriangle(sideA, sideB, sideC));
			}

			public static boolean ValidTriangle(double sideA, double sideB, double sideC) {
				return (sideA + sideB > sideC) && (sideA + sideC > sideB) && (sideB + sideC > sideA);
			}
		}

/*
 SIDE A: 10
SIDE B: 11
SIDE C: 12
Is Validity Triangle : true
 */
		 