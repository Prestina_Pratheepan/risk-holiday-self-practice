package holiday.self.practicesQ28;

import java.util.Scanner;

public class StringReverse {
	
		 public static void main(String args[]){
			   
			   Scanner scan  = new Scanner(System.in);
			   System.out.print("Enter an input String : ");
			   String real = scan.nextLine();
			    
			   String reverse = "";
			   int length = real.length();

			    for (int i = length - 1 ; i >= 0 ; i--) {
			    	reverse = reverse + real.charAt(i);
			    }
			    	System.out.println("The reverse of "+ real +" is " + reverse);
		}
	}

/*
Enter an input String : Computer Programming
The reverse of Computer Programming is gnimmargorP retupmoC
*/

/*
Enter an input String : prestina
The reverse of prestina is anitserp
*/