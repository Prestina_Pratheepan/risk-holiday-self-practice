package holiday.self.practicesQ14;
import java.util.Scanner;
public class AllPositive {
	
	public static void main(String[] args) {
		
			Scanner scan = new Scanner(System.in);
			
			System.out.print("ENTER VALUE A : ");
			double ValueA = scan.nextInt();
			System.out.print("ENTER VALUE B : ");
			double ValueB = scan.nextInt();
			System.out.print("ENTER VALUE C : ");
			double ValueC = scan.nextInt();
			System.out.print("Are All The Values Positive : " + AllPositive(ValueA, ValueB, ValueC));
		}

		public static boolean AllPositive (double ValueA, double ValueB, double ValueC) {
			return (ValueA > 0) && (ValueB > 0) && (ValueC > 0);
		}
}

/*
ENTER VALUE A : 10
ENTER VALUE B : 11
ENTER VALUE C : 12
Are All The Values Positive : true
*/