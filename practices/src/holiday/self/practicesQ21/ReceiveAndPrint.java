package holiday.self.practicesQ21;
import java.util.Scanner;

public class ReceiveAndPrint {
		
	public static void main(String args[]) {
			Scanner scan = new Scanner(System.in);
			
			System.out.print("ENTER STRING :");
			String s = scan.nextLine();
			System.out.print("ENTER INTEGER : ");
			int m = scan.nextInt();
			System.out.print("ENTER DOUBLE : ");
			double d = scan.nextDouble();
			
			System.out.println("S: " +s);
			System.out.println("M: " +m);
			System.out.println("D: " +d);
			System.out.println("M+D+S: " +m+d+s);
			System.out.println("M+S+D: " +m+s+d);
			System.out.println("S+M+D: " +s+m+d);
		}
	}

/*
ENTER STRING : prestina
ENTER INTEGER : 2
ENTER DOUBLE : 4.2
S: prestina
M: 2
D: 4.2
M+D+S: 24.2prestina
M+S+D: 2prestina4.2
S+M+D: prestina24.2
 */
