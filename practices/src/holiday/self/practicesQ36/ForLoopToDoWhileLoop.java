package holiday.self.practicesQ36;

public class ForLoopToDoWhileLoop {

	public static void main(String args[]) {
			
			int index = 1;
			
			do {
				
				System.out.println(index);
				index=index + 3;
				
				
			} while (index <= 15);
		}
	}

	
/*
1
4
7
10
13
*/