package holiday.self.practicesQ09;

public class PlayWithNumbersDecomposedDemo {
	
			public static void main(String[] args) {
				int a = 0;
				int b = 0;
				int c = 0;
				
				PlayWithNumbersDecomposed demo = new PlayWithNumbersDecomposed();
			
				demo.program1(a,b);
				demo.program2(a,b,c);
			}
		}

/*
 * 
 Enter two integers:  
1000435
345
a + b IS EQUAL TO 1000780
a - b IS EQUAL TO 1000090
a * b IS EQUAL TO 345150075
a / b IS EQUAL TO 2899
a % b IS EQUAL TO 280

Enter three integers:  
34325
79
-40
(a - b)/c IS EQUAL TO -856
(a - c)/b IS EQUAL TO 435
(b - c)/a IS EQUAL TO 0
(b - a)/c IS EQUAL TO 856
(c - a)/b IS EQUAL TO -435
(c - b)/a IS EQUAL TO 0
 */


		

