package holiday.self.practicesQ08;

public class Trapezoid {
	
	public static void main(String[] args) {
	
	double top = 10;
	double bottom = 8;
	double height = 7;
	double area = (bottom+top)*height/2;
	
	System.out.print("Top : ");
	System.out.println(top);
	System.out.print("Bottom : ");
	System.out.println(bottom);
	System.out.print("Height : ");
	System.out.println(height);
	System.out.print("Area : ");
	System.out.println(area);
}
}

/*
Top : 10.0
Bottom : 8.0
Height : 7.0
Area : 63.0
*/