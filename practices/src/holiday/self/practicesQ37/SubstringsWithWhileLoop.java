package holiday.self.practicesQ37;

public class SubstringsWithWhileLoop {

public static void main(String[] args) {
	
			String name = "sebastian-ibis";
			
			int length = name.length();
			int i = length - 1;
			
			
			while (i > 0) {
				System.out.println(name.substring(i));
				i = i -1;
		
			}
		}
	}
	
/*
s
is
bis
ibis
-ibis
n-ibis
an-ibis
ian-ibis
tian-ibis
stian-ibis
astian-ibis
bastian-ibis
ebastian-ibis
	 */


