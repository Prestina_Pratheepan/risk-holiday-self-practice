package holiday.self.practicesQ26;

public class PrintingtheSuffixofaStringVariable {
	
	public static void main(String[] args) {
		
			String name = "hurricanes";
			int length = name.length();
			
			for (int row = 0; row <= length; row++) {
				for (int i = row; i <= row; i++) {
					System.out.print(name.substring(i));
				}
				System.out.println();
			}
		}
	}

/*
hurricanes
urricanes
rricanes
ricanes
icanes
canes
anes
nes
es
s


 */


