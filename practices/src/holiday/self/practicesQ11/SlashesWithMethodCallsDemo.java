package holiday.self.practicesQ11;

public class SlashesWithMethodCallsDemo {

	public static void main(String[] args) {
			int p = 0;
			int s = 0;
			
			SlashesWithMethodCalls demo = new SlashesWithMethodCalls();
			
			demo.slashes();
			demo.slashes();
			demo.slashes();
			demo.slashes();
			demo.slashes();
			
		}
	}
	
/*
 / / / / / / / / / / / / 
 / / / / / / / / / / /
/ / / / / / / / / / / / 
 / / / / / / / / / / /
/ / / / / / / / / / / / 
 / / / / / / / / / / /
/ / / / / / / / / / / / 
 / / / / / / / / / / /
/ / / / / / / / / / / / 
 / / / / / / / / / / /
 */
