package holiday.self.practicesQ04;

public class Slashes {

public static void main (String args []) {
		
		 int lines=10;
		 
			for (int i = 1; i <= lines; i++) {
				System.out.println(i + " ");
				
				if (i % 2 == 1) {
					
					for (int j = 1; j <= (lines +2); j++) {
						System.out.print(  " / ");
						
						if (j % (lines + 2) == 0) {
							System.out.println(" ");
						}
					}
				} 
				
				else {
					for (int p = 1; p <= (lines + 1); p++) {
						System.out.print(" / ");
						
						if (p % (lines + 1) == 0) {
							System.out.println(" "); 
						}
					}
}
			}
	}
}

/*
 1 
 /  /  /  /  /  /  /  /  /  /  /  /  
2 
 /  /  /  /  /  /  /  /  /  /  /  
3 
 /  /  /  /  /  /  /  /  /  /  /  /  
4 
 /  /  /  /  /  /  /  /  /  /  /  
5 
 /  /  /  /  /  /  /  /  /  /  /  /  
6 
 /  /  /  /  /  /  /  /  /  /  /  
7 
 /  /  /  /  /  /  /  /  /  /  /  /  
8 
 /  /  /  /  /  /  /  /  /  /  /  
9 
 /  /  /  /  /  /  /  /  /  /  /  /  
10 
 /  /  /  /  /  /  /  /  /  /  / 
 */




