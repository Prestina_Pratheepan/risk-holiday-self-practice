package holiday.self.practicesQ33;

public class WhileLoopForPrintingNumbersWithLeading {
	
	public static void main(String args[]) {
		
			int num = 20;
			int i = 1;
			
			while (i <= num/2) {
				
				int even=i*2;
				
				String leading=String.format("%04d", even);
				
				System.out.println(leading);
				i = i + 1;
			}
		}
	}

/*OUTPUT
0002
0004
0006
0008
0010
0012
0014
0016
0018
0020
*/
	


