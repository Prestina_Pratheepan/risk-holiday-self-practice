package holiday.self.practicesQ02;

public class DiamondFilled {

public static void main(String[] args){
	
    	int lines = 5;
    	
	        for (int i = 1; i <= lines; i++){
	            for (int j = 1; j < lines-i; j++){
	                System.out.print(" ");
	            }
	            for (int p = 1; p <= i; p++){
	                System.out.print("/");
	                System.out.print("");
	            } 
	            for (int j = 0; j < i; j++){
	            	System.out.print("\\");
		     	}
		     	  	System.out.println();
	         }
	        for (int i = lines; i >= 1 ; i--){
	        	for (int j = lines; j > i;j--){
	        		System.out.print(" ");
	     	    }
	     	    for (int p = 1; p <= i; p++){
	     	    	System.out.print("\\");
	     	    }
	     	    	System.out.print("");
	     	    for (int j = 0; j < i; j++){
	     	    	System.out.print("/");
	     	    }
	     	    	System.out.println();
	        }
}
}
    


/*
   /\
  //\\
 ///\\\
////\\\\
/////\\\\\
\\\\\/////
 \\\\////
  \\\///
   \\//
    \/
 */
 

